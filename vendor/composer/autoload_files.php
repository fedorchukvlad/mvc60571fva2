<?php

// autoload_files.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    '045e6192462dfbc79d72b15e0566f7af' => $baseDir . '/app/config.php',
    '12485bf065bdb9fbdb827ff54f4cb2f9' => $baseDir . '/app/routes.php',
);
